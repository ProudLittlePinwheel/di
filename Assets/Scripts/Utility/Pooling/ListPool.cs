﻿using System;
using System.Collections.Generic;

/// <summary>
/// Simple generic object pool class for lists. Allows you to take an object out of the pool ('Get') and 'Return' it when finished.
/// </summary>
/// <typeparam name="T"></typeparam>
public static class ListPool<T>
{
	private static readonly Pool<List<T>> pool = new Pool<List<T>>(null, l => l.Clear());

	public static List<T> Get()
	{
		return pool.Get();
	}

	public static void Return(List<T> toRelease)
	{
		pool.Return(toRelease);
	}
}