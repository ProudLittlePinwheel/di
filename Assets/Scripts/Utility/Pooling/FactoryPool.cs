﻿using System;
using System.Collections.Generic;

/// <summary>
/// Simple generic object pool class. Allows you to take an object out of the pool ('Get') and 'Return' it when finished.
/// </summary>
/// <typeparam name="T"></typeparam>
public class FactoryPool<T>
{
	private Stack<T> mPool = new Stack<T>();
	private Action<T> onGet, onReturn;
	private Func<T> factory;

	public FactoryPool(Func<T> factory, Action<T> onGet, Action<T> onReturn)
	{
		this.factory = factory;
		this.onGet = onGet;
		this.onReturn = onReturn;
	}

	/// <summary>
	/// Returns an object off the pool stack or creates a new one. Calls onGet action on that object.
	/// </summary>
	/// <returns></returns>
	public T Get()
	{
		// pop or allocate
		T obj;
		if (mPool.Count == 0)
		{
			obj = factory();
		}
		else
		{
			obj = mPool.Pop();
		}

		if(onGet != null)
		{
			onGet(obj);
		}

		return obj;
	}

	/// <summary>
	/// Return an object to the pool when finished with. Calls onReturn action on it.
	/// </summary>
	/// <param name="obj"></param>
	public void Return(T obj)
	{
		if(onReturn != null)
		{
			onReturn(obj);
		}

		mPool.Push(obj);
	}
}