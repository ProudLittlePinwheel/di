﻿using System;
using System.Collections.Generic;

/// <summary>
/// Simple generic object pool class. Allows you to take an object out of the pool ('Get') and 'Return' it when finished.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Pool<T> where T : new()
{
	private Stack<T> mPool = new Stack<T>();
	private Action<T> onGet, onReturn;

	public Pool(Action<T> onGet, Action<T> onReturn)
	{
		this.onGet = onGet;
		this.onReturn = onReturn;
	}

	/// <summary>
	/// Returns an object off the pool stack or creates a new one. Calls onGet action on that object.
	/// </summary>
	/// <returns></returns>
	public T Get()
	{
		// pop or allocate
		T obj;
		if (mPool.Count == 0)
		{
			obj = new T();
		}
		else
		{
			obj = mPool.Pop();
		}

		if (onGet != null)
		{
			onGet(obj);
		}

		return obj;
	}

	/// <summary>
	/// Return an object to the pool when finished with. Calls onReturn action on it.
	/// </summary>
	/// <param name="obj"></param>
	public void Return(T obj)
	{
		if (onReturn != null)
		{
			onReturn(obj);
		}

		mPool.Push(obj);
	}

	private static Pool<T> mStaticPool;

	/// <summary>
	/// Returns an object off the pool stack or creates a new one. 
	/// <para/>This static call creates a Pool if it does not yet exist for objects of type {T}.
	/// </summary>
	/// <returns></returns>
	public static U Get<U>() where U : T
	{
		if (mStaticPool == null)
			mStaticPool = new Pool<T>(null, null);
		return (U)(mStaticPool).Get();
	}

	/// <summary>
	/// Return an object to the pool when finished with. Calls onReturn action on it.
	/// <para/>This static call creates a Pool if it does not yet exist for objects of type {T}.
	/// </summary>
	/// <param name="obj"></param>
	public static void Return<U>(U obj) where U : T
	{
		if (mStaticPool == null)
			mStaticPool = new Pool<T>(null, null);
		(mStaticPool).Return((T)obj);
	}
}