﻿using System;

public static class SystemRandomExtensions
{
	public static int Range(this Random Random, int min, int max)
	{
		float pctg = (float)Random.NextDouble();
		return min + (int)Math.Round((max - min) * pctg);
	}

	public static float Range(this Random Random, float min, float max)
	{
		float pctg = (float)Random.NextDouble();
		return min + ((max - min) * pctg);
	}

	public static float Value(this Random Random)
	{
		return (float)Random.NextDouble();
	}
}