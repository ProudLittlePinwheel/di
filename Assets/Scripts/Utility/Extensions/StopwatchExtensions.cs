public static class StopwatchExtensions
{
	public static long ElapsedNanoSeconds(this System.Diagnostics.Stopwatch watch)
	{
		return watch.ElapsedTicks * 1000000000L / System.Diagnostics.Stopwatch.Frequency;
	}
	public static long ElapsedMicroSeconds(this System.Diagnostics.Stopwatch watch)
	{
		return watch.ElapsedTicks * 1000000L / System.Diagnostics.Stopwatch.Frequency;
	}
	public static double ElapsedMicroSecondsD(this System.Diagnostics.Stopwatch watch)
	{
		return ((double)ElapsedNanoSeconds(watch)) / 1000d;
	}
	public static double ElapsedMilliSecondsD(this System.Diagnostics.Stopwatch watch)
	{
		return ElapsedMicroSecondsD(watch) / 1000d;
	}
}