using System;
using System.Collections.Generic;

namespace pod.utility
{
	public static class ArrayListExtensions
	{
		public static List<T> ToList<T>(this T[] array)
		{
			if (array == null) return null;
			List<T> list = new List<T>(array.Length);
			for (int i = 0; i < array.Length; ++i) list.Add(array[i]);
			return list;
		}
		public static void ToList<T>(this T[] array, List<T> list)
		{
			if (array == null) return;
			if (list == null) return;
			else list.Clear();
			for (int i = 0; i < array.Length; ++i)
				list.Add(array[i]);
		}

		public static bool Contains<T>(this T[] array, T element)
		{
			return Array.IndexOf(array, element) >= 0;
		}
		public static bool Contains<T>(this IList<T> ilist, T element)
		{
			return ilist.IndexOf(element) >= 0;
		}

		public static bool Contains<T>(this T[] array, Predicate<T> predicate)
		{
			return Array.FindIndex(array, predicate) >= 0;
		}
		public static bool Contains<T>(this IList<T> ilist, Predicate<T> predicate)
		{
			for (int i = 0; i < ilist.Count; ++i)
			{
				if (predicate(ilist[i]))
					return true;
			}
			return false;
		}

		public static T Find<T>(this T[] array, T element)
		{
			int index = Array.IndexOf(array, element);
			if (index != -1)
				return array[index];
			return default(T);
		}
		public static T Find<T>(this IList<T> ilist, T element)
		{
			int index = ilist.IndexOf(element);
			if (index != -1)
				return ilist[index];
			return default(T);
		}

		public static T Find<T>(this T[] array, T element, T defaultValue)
		{
			int index = Array.IndexOf(array, element);
			if (index != -1)
				return array[index];
			return defaultValue;
		}
		public static T Find<T>(this IList<T> ilist, T element, T defaultValue)
		{
			int index = ilist.IndexOf(element);
			if (index != -1)
				return ilist[index];
			return defaultValue;
		}

		public static T Find<T>(this IList<T> ilist, Predicate<T> predicate)
		{
			for (int i = 0; i < ilist.Count; ++i)
			{
				if (predicate(ilist[i]))
					return ilist[i];
			}
			return default(T);
		}

		public static T Find<T>(this IList<T> ilist, Predicate<T> predicate, T defaultValue)
		{
			for (int i = 0; i < ilist.Count; ++i)
			{
				if (predicate(ilist[i]))
					return ilist[i];
			}
			return defaultValue;
		}

		public static T First<T>(this T[] array)
		{
			return array[0];
		}
		public static T First<T>(this IList<T> ilist)
		{
			return ilist[0];
		}

		public static T Last<T>(this T[] array)
		{
			return array[array.Length - 1];
		}
		public static T Last<T>(this IList<T> ilist)
		{
			return ilist[ilist.Count - 1];
		}

		public static void Execute<T>(this T[] array, Action<T> action)
		{
			for (int i = 0; i < array.Length; ++i)
			{
				action(array[i]);
			}
		}
		public static void Execute<T>(this IList<T> ilist, Action<T> action)
		{
			for (int i = 0; i < ilist.Count; ++i)
			{
				action(ilist[i]);
			}
		}

		public static int Increment<T>(this T[] array, int index)
		{
			if (array == null)
				return -1;

			if (index + 1 >= array.Length)
			{
				return 0;
			}
			return index + 1;
		}
		public static int Increment<T>(this IList<T> list, int index)
		{
			if (list == null)
				return -1;

			if (index + 1 >= list.Count)
			{
				return 0;
			}
			return index + 1;
		}

		public static int Decrement<T>(this T[] array, int index)
		{
			if (array == null)
				return -1;

			if (index - 1 < 0)
			{
				return array.Length - 1;
			}
			return index - 1;
		}
		public static int Decrement<T>(this IList<T> list, int index)
		{
			if (list == null)
				return -1;

			if (index - 1 < 0)
			{
				return list.Count - 1;
			}
			return index - 1;
		}

		public static void Populate<T>(this List<T> list, params T[] args)
		{
			list.AddRange(args);
		}

		public static void Populate<T>(this IList<T> list, params T[] args)
		{
			for (int i = 0; i < args.Length; ++i)
				list.Add(args[i]);
		}

		public static void Add<T>(this List<T> list, T arg1, T arg2)
		{
			list.Add(arg1);
			list.Add(arg2);
		}

		public static void Add<T>(this IList<T> list, T arg1, T arg2)
		{
			list.Add(arg1);
			list.Add(arg2);
		}

		public static void Add<T>(this List<T> list, T arg1, T arg2, T arg3)
		{
			list.Add(arg1);
			list.Add(arg2);
			list.Add(arg3);
		}

		public static void Add<T>(this IList<T> list, T arg1, T arg2, T arg3)
		{
			list.Add(arg1);
			list.Add(arg2);
			list.Add(arg3);
		}

		public static void Add<T>(this List<T> list, T arg1, T arg2, T arg3, T arg4)
		{
			list.Add(arg1);
			list.Add(arg2);
			list.Add(arg3);
			list.Add(arg4);
		}

		public static void Add<T>(this IList<T> list, T arg1, T arg2, T arg3, T arg4)
		{
			list.Add(arg1);
			list.Add(arg2);
			list.Add(arg3);
			list.Add(arg4);
		}

		public static TOutput[] ToArray<T, TOutput>(this IList<T> list, Converter<T, TOutput> converter)
		{
			TOutput[] output = new TOutput[list.Count];
			for(int i = 0; i < list.Count; ++i)
			{
				output[i] = converter(list[i]);
			}
			return output;
		}

        public static void AddRange<T, U>(this List<T> list, List<U> range) where U : T
        {
            if (range == null)
                return;
            for (int i = 0; i < range.Count; ++i)
            {
                list.Add((U)range[i]);
            }
        }
	}
}