﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System.Collections.Generic;

namespace pod.dependancy_injection
{
	/// <summary>
	/// Able to consume a list of <see cref="ISharedPtr"/>(s).
	/// </summary>
	public interface IConsumer
	{
		/// <summary>
		/// Injects the specified <see cref="ISharedPtr"/>(s) into this <see cref="IConsumer"/>.
		/// </summary>
		/// <param name="ptrs"></param>
		void Inject(IList<ISharedPtr> ptrs);
	}
}
