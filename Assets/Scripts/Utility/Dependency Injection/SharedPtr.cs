﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections.Generic;
using UnityEngine;

namespace pod.dependancy_injection
{
	// A WIP injection framwork based on interfaces rather than attributes and reflection.
	public static class SharedPtr
	{
        /// <summary>
        /// Create a pointer to the reference.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="owner"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
		public static SharedPtr<T> Create<T>(object owner, T target) where T : class, IInjectable
		{
			return SharedPtr<T>.Create(owner, target);
		}

        /// <summary>
        /// Create a pointer with the intention to bind the reference later before first use.
        /// <para/>Use <see cref="BindTarget{T}(object, T, SharedPtr{T})"/> to bind the reference.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="owner"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
		public static SharedPtr<T> CreateDeferred<T>(object owner) where T : class, IInjectable
		{
			return UnsafeSharedPtrAccess.CreateNullTarget<T>(owner);
		}

        /// <summary>
        /// Bind a reference to a shared pointer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="user"></param>
        /// <param name="target"></param>
        /// <param name="ptr"></param>
        /// <returns></returns>
		public static SharedPtr<T> BindTarget<T>(object user, T target, SharedPtr<T> ptr) where T : class, IInjectable
		{
			if ((object)ptr == null)
				ptr = SharedPtr<T>.Create(user, target);
			else
				UnsafeSharedPtrAccess.BindTarget(user, target, ptr);
			return ptr;
		}

		private interface IExampleShared : IInjectable
		{
			void DoSomething();
		}
		private class ExampleShared : IExampleShared
		{
			public void DoSomething() { }
		}

		private static void Example()
		{
			object owner = new object();
			object user = new object();

			// Create a shared pointer
			SharedPtr<IExampleShared> ptr = SharedPtr.Create(owner, (IExampleShared)new ExampleShared());

			// Pointer is bound to owner so we can de-reference using owner and call DoSomething
			ptr[owner].DoSomething();

			// This call will throw an exception since 'user' is not a valid user of ptr
			ptr[user].DoSomething();

			// To make it a valid user we call BindUser
			ptr.BindUser(user);

			// Now we can do stuff with the ptr by de-referencing with respect to user
			ptr[user].DoSomething();

			// This includes being able to delete the ptr
			ptr.Delete(user); // This call will indicate the outstanding users of object to help with debugging

			// This call will throw an exception since the pointer is deleted
			ptr[owner].DoSomething();

			// You can check to see if this is null, an overload allows returning true if the SharedPtr container isn't null but its target is
			Debug.Assert(ptr == null); // Will return true since we deleted the ptr contents

			// You can query the lifespan of the ptr (set during construction)
			Debug.Log(ptr.Lifespan);

			// You can cast a SharedPtr to an ISharedPtr for generic handling, which supports determining underlying type and attempting casting from generic interface
#pragma warning disable 0219 // The variable `iptr' is assigned but its value is never used
			ISharedPtr iptr = ptr;
#pragma warning restore 0219 // The variable `iptr' is assigned but its value is never used

			// Finally, you can undo all this hard work by accessing the underlying reference yourself using unsafe access
			UnsafeSharedPtrAccess.RawPtr(ptr).DoSomething(); // Warning: this performs no null checking...

			// If you ever store a reference to the underlying SharedPtr value, you may cause a memory leak as you may hold the reference when you aren't meant to
			// This is what we are trying to avoid so don't do this unless you are caching to a local stack variable which will go out of scope at the end of the function call
			// Basically, just like in c++, don't store the underlying ptr value, use it through the sharedPointer
			IExampleShared exampleShared = ptr[owner];
		}

		private static Pool<List<ISharedPtr>> ptrs = new Pool<List<ISharedPtr>>(null, list => list.Clear());
        /// <summary>
        /// Inject pointers supplied by the provider into the consumer.
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="consumer"></param>
		public static void Inject(this IProvider provider, IConsumer consumer)
		{
			var list = ptrs.Get();
			provider.Provide(list);
			consumer.Inject(list);
			ptrs.Return(list);
		}

        /// <summary>
        /// Inject pointers supplied by the enumerable into the consumer.
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="consumer"></param>
		public static void Inject(this IEnumerable<ISharedPtr> provider, IConsumer consumer)
		{
			var list = ptrs.Get();
			list.AddRange(provider);
			consumer.Inject(list);
			ptrs.Return(list);
		}

		private class Map : IDisposable
		{
			private Dictionary<Type, ISharedPtr> ptrMap = new Dictionary<Type, ISharedPtr>();

			public ISharedPtr this[Type type]
			{
				get { return ptrMap[type]; }
			}

			public void Populate(IList<ISharedPtr> ptrs)
			{
				for (int i = 0; i < ptrs.Count; ++i)
				{
					ptrMap.Add(ptrs[i].Type, ptrs[i]);
				}
			}

			private void Example()
			{
				object user = null;
				SharedPtr<IExampleShared> examplePtr = null; // New ptr

				examplePtr = ptrMap[examplePtr].Cast<IExampleShared>().BindUser(user);
				// or
				examplePtr = ptrMap[examplePtr].Cast(examplePtr).BindUser(user);
			}

			public void Dispose()
			{
				ptrMap.Clear();
				Pool<Map>.Return<Map>(this);
			}

			public static Map Get()
			{
				return Pool<Map>.Get<Map>();
			}
		}

		public static void Inject<T>(this IProvider provider, object consumer, out SharedPtr<T> ptr) where T : class, IInjectable
		{
			var list = ptrs.Get();
			provider.Provide(list);
			Inject(list, consumer, out ptr);
		}

		public static void Inject<T>(this IEnumerable<ISharedPtr> provider, object consumer, out SharedPtr<T> ptr) where T : class, IInjectable
		{
			var list = ptrs.Get();
			list.AddRange(provider);
			Inject(list, consumer, out ptr);
		}

		private static void Inject<T>(List<ISharedPtr> list, object consumer, out SharedPtr<T> ptr) where T : class, IInjectable
		{
			using (var map = Map.Get())
			{
				map.Populate(list);

				ptr = map[typeof(T)].Cast<T>();
				if (ptr != null) ptr.BindUser(consumer);
			}
			ptrs.Return(list);
		}

		public static void Inject<T1, T2>(this IProvider provider, object consumer, 
			
			out SharedPtr<T1> ptr1, 
			out SharedPtr<T2> ptr2)

			where T1 : class, IInjectable
			where T2 : class, IInjectable
		{
			var list = ptrs.Get();
			provider.Provide(list);
			Inject(provider, consumer, out ptr1, out ptr2);
		}

		public static void Inject<T1, T2>(this IEnumerable<ISharedPtr> provider, object consumer,

			out SharedPtr<T1> ptr1,
			out SharedPtr<T2> ptr2)

			where T1 : class, IInjectable
			where T2 : class, IInjectable
		{
			var list = ptrs.Get();
			list.AddRange(provider);
			Inject(provider, consumer, out ptr1, out ptr2);
		}


		public static void Inject<T1, T2>(List<ISharedPtr> list, object consumer,

			out SharedPtr<T1> ptr1,
			out SharedPtr<T2> ptr2)

			where T1 : class, IInjectable
			where T2 : class, IInjectable
		{
			using (var map = Map.Get())
			{
				map.Populate(list);

				ptr1 = map[typeof(T1)].Cast<T1>();
				if (ptr1 != null) ptr1.BindUser(consumer);

				ptr2 = map[typeof(T2)].Cast<T2>();
				if (ptr2 != null) ptr2.BindUser(consumer);
			}
			ptrs.Return(list);
		}

		public static void Inject<T1, T2, T3>(this IProvider provider, object consumer,

			out SharedPtr<T1> ptr1,
			out SharedPtr<T2> ptr2,
			out SharedPtr<T3> ptr3)

			where T1 : class, IInjectable
			where T2 : class, IInjectable
			where T3 : class, IInjectable
		{
			var list = ptrs.Get();
			provider.Provide(list);

			using (var map = Map.Get())
			{
				map.Populate(list);

				ptr1 = map[typeof(T1)].Cast<T1>();
				if (ptr1 != null) ptr1.BindUser(consumer);

				ptr2 = map[typeof(T2)].Cast<T2>();
				if (ptr2 != null) ptr2.BindUser(consumer);

				ptr3 = map[typeof(T3)].Cast<T3>();
				if (ptr3 != null) ptr3.BindUser(consumer);
			}

			ptrs.Return(list);
		}

		public static void Inject<T1, T2, T3, T4>(this IProvider provider, object consumer,

			out SharedPtr<T1> ptr1,
			out SharedPtr<T2> ptr2,
			out SharedPtr<T3> ptr3,
			out SharedPtr<T4> ptr4)

			where T1 : class, IInjectable
			where T2 : class, IInjectable
			where T3 : class, IInjectable
			where T4 : class, IInjectable
		{
			var list = ptrs.Get();
			provider.Provide(list);

			using (var map = Map.Get())
			{
				map.Populate(list);

				ptr1 = map[typeof(T1)].Cast<T1>();
				if (ptr1 != null) ptr1.BindUser(consumer);

				ptr2 = map[typeof(T2)].Cast<T2>();
				if (ptr2 != null) ptr2.BindUser(consumer);

				ptr3 = map[typeof(T3)].Cast<T3>();
				if (ptr3 != null) ptr3.BindUser(consumer);

				ptr4 = map[typeof(T4)].Cast<T4>();
				if (ptr4 != null) ptr4.BindUser(consumer);
			}

			ptrs.Return(list);
		}
	}
}