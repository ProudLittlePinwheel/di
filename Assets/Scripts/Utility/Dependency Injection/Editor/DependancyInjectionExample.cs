﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
#pragma warning disable 0219, 414

using System;
using System.Collections;
using System.Collections.Generic;

namespace pod.dependancy_injection.example
{
    // Ensure to include di framework in what you are doing
    using pod.dependancy_injection;

    public class DependancyInjectionExample
    {
        public static void Run()
        {
            Singleton.Editor.Reset();
            new DependancyInjectionExample().Init();
        }

        private Singleton<IExampleSingleton> exampleSingleton;
        private void Init()
        {
            // Create a singleton as an interface
            exampleSingleton = Singleton.Create(new ExampleSingleton() as IExampleSingleton);

            // Use the singleton
            exampleSingleton.Instance.Foo();

            // This user automatically hooks the singleton for internal use within its constructor
            var singletonUser = new ExampleSingletonUser();

            // Create something that provides some objects
            var provider = new EnumerableProvider();

            // Create something that needs access to these objects
            var consumer = new Consumer(provider);

            // Use what has been injected
            consumer.UsePointers();

            // Cleanup provider
            provider.Dispose();
        }
    }

    // Singleton

    public interface IExampleSingleton : IInjectable
    {
        // This interface itself must be an IInjectable to be useable as a singleton. 
        // This ensures that inheriting classes are IInjectables too.

        void Foo();
    }

    public class ExampleSingleton : IExampleSingleton
    {
        public void Foo() { }
    }

    public class ExampleSingletonUser
    {
        private Singleton<IExampleSingleton> exampleSingleton;

        public ExampleSingletonUser()
        {
            Singleton.Inject(this, out exampleSingleton);
        }
    }

    // Singleton + SharedPtr

    public class EnumerableProvider : IEnumerable<ISharedPtr>, IDisposable
    {
        private SharedPtr<IFoo> foo;
        private SharedPtr<IBar> bar;

        public EnumerableProvider()
        {
            // Create shared pointers in constructor
            foo = SharedPtr.Create(this, new Foo() as IFoo);
            bar = SharedPtr.Create(this, new Bar() as IBar);
        }

        public void Dispose()
        {
            foo.Delete(this);
            bar.Delete(this);
        }

        public IEnumerator<ISharedPtr> GetEnumerator()
        {
            // Enumerate the shared pointers that we supply
            yield return foo;
            yield return bar;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public interface IRun { void Run(); }

    public interface IFoo : IInjectable, IRun { }
    public class Foo : IFoo
    {
        public void Run() { }
    }

    public interface IBar : IInjectable, IRun { }
    public class Bar : IBar
    {
        public void Run() { }
    }

    public class Consumer
    {
        private Singleton<IExampleSingleton> singleton;
        private SharedPtr<IFoo> foo;
        private SharedPtr<IBar> bar;

        public Consumer(IEnumerable<ISharedPtr> provider)
        {
            // Inject a singleton
            Singleton.Inject(this, out singleton);
            // Inject provided objects
            provider.Inject(this, out foo, out bar);
        }

        public void UsePointers()
        {
            // Use singleton
            singleton.Instance.Foo();
            // Use provided objects (dereference with respect to 'this')
            foo[this].Run();
            bar[this].Run();
        }
    }
}