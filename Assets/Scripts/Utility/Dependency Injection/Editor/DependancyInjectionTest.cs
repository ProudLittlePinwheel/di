﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;
using System.Collections.Generic;

using pod.utility;
using System.Collections;

namespace pod.dependancy_injection.Testing
{
    public class DependancyInjectionTest
    {
        [Test(Description = "Tests SharedPtr creation, injection, destruction, etc...")]
        public void RunTest()
        {
            Singleton.Editor.Reset(); // We must reset the singletons because this is an editor test (static vars not reloaded)

            Provider provider = new Provider();

            // Validate provider's SharedPtr
            Assert.IsNotNull(provider.injectable);
            Assert.IsNotNull(provider.injectable[provider]);
            Assert.IsTrue(provider.injectable[provider].DoSomething());

            // Create a consumer that takes a provider as input
            Consumer consumer = new Consumer(provider);

            // Test the ability to create using enumerators
            List<ISharedPtr> ptrs = new List<ISharedPtr>(consumer);
            Assert.IsTrue(ptrs.Count == 2);

            // Validate consumer's SharedPtr
            Assert.IsTrue(consumer.injectable != null);
            Assert.IsNotNull(consumer.injectable);
            Assert.IsNotNull(consumer.injectable[consumer]);
            Assert.IsTrue(consumer.injectable[consumer].DoSomething());

            // Validate that the ptrs are the same (they are not actually the same object, but the things they point to are the same, so this should return true)
            Assert.IsTrue(consumer.injectable == consumer.injectable1);

            // Check that they point to the same thing by trying to dereference them in creative ways
            Assert.IsNotNull(provider.injectable[consumer]);
            Assert.IsNotNull(consumer.injectable[provider]);

            // Should throw since we are dereferencing using an invalid user
            Assert.Throws(typeof(InvalidSharedPtrUserException), () => provider.injectable[new object()].DoSomething());

            // Test unsafe access, for when it is neccessary
            Assert.IsInstanceOf(typeof(ExampleInjectable), UnsafeSharedPtrAccess.RawPtr(provider.injectable));
            Assert.DoesNotThrow(() => UnsafeSharedPtrAccess.RawPtr(provider.injectable).DoSomething());

            // Test the inner workings of the AltConsumer class, which uses the ptr internally
            Assert.DoesNotThrow(() => { using (var ac = new AltConsumer(provider)) { } });

            // Force a collection, which will re-claim the floating internal reference within AltConsumer (it was nulled, but not collected yet)
            GC.Collect();

            // Delete the internal object (can be invoked from anyone, not just the original owner/creator)
            // Please note that this will wipe out the user binding information too
            Assert.DoesNotThrow(() => consumer.injectable.Delete(provider));

            Assert.Throws(typeof(MissingReferenceException), () => provider.injectable[provider].DoSomething());

            /// TODO: Should these throw, or should they return null? (i.e. Should the exception only be thrown if you try to use it?)
            // Argument: They should throw since we are essentially dereferencing a bollocks pointer, which is equivalent to bad memory access...
            Assert.Throws(typeof(MissingReferenceException), () => { var i = provider.injectable[provider]; });
            Assert.Throws(typeof(MissingReferenceException), () => { var i = consumer.injectable[consumer]; });
            Assert.Throws(typeof(MissingReferenceException), () => { var i = consumer.injectable1[consumer]; });

            // Test overloaded == operator checking
            Assert.IsTrue(provider.injectable == null);
            Assert.IsTrue(provider.injectable == (object)null);

            // This is a unique case, the ptr will not be resolved to null as it is cast to an object and hence the overriden == is not invoked, just the regular object.ReferenceEquals
            // Therefore it will resolve to false even though the previous two calls resolved to true
            Assert.IsFalse((object)provider.injectable == null);

            // Show that a pointer can be re-bound at runtime
            // Please Bear in mind that the delete call earlier wiped out the user bindings, so you would have to re-create those
            /// TODO: Should delete calls wipe out binding information...?
            /// If not then require user to clear manually, via Dispose in order to pool correctly, otherwise will be reclaimed by GC (requires move away from Dict implementation)
            Assert.DoesNotThrow(() => SharedPtr.BindTarget(provider, new ExampleInjectable(), provider.injectable));
            Assert.IsTrue(provider.injectable[provider].DoSomething());

            // Show that a shared pointer can be created without a target... (i.e. deferred injection support)
            SharedPtr<ExampleInjectable> deferred = SharedPtr.CreateDeferred<ExampleInjectable>(provider);
            Assert.IsNull(UnsafeSharedPtrAccess.RawPtr(deferred));

            // Show that you can bind to a null pointer
            Assert.DoesNotThrow(() => deferred.BindUser(consumer));

            // Show that you can bind deferred pointer (i.e. consumer populates ptr after provider created ptr)
            Assert.DoesNotThrow(() => SharedPtr.BindTarget(consumer, new ExampleInjectable(), deferred));
            Assert.IsTrue(deferred[consumer].DoSomething());

            // Show that you cannot bind pointer with invalid user
            Assert.Throws(typeof(InvalidSharedPtrUserException), () => SharedPtr.BindTarget(new object(), new ExampleInjectable(), deferred));

            SharedPtr<ExampleInjectable> exampleSingleton = SharedPtr.Create(Singleton.Provider, new ExampleInjectable());
            //Singleton.Provider.Inject(null, )
            exampleSingleton[Singleton.Provider].DoSomething();

            // This will throw as a singleton of this type already exists
            Assert.Throws(typeof(SingletonException), () => { Singleton<ExampleInjectable> sing2 = Singleton.Create(new ExampleInjectable()); sing2.Instance.DoSomething(); });

            Singleton<ExampleDeferredSingleton> deferredSingleton;
            // Here we declare that we want accesss to a singleton that doesn't exist yet...;
            Singleton.Inject(this, out deferredSingleton);
            
            // And later we bind that singleton
            Singleton<ExampleDeferredSingleton> deferredSingletonInst = Singleton.Create(new ExampleDeferredSingleton());

            Assert.AreSame(deferredSingletonInst.Instance, deferredSingleton.Instance);
        }

        private class ExampleInjectable : IInjectable
        {
            public bool DoSomething() { return true; }
        }

        private class ExampleDeferredSingleton : IInjectable
        {
            public bool DoSomething() { return true; }
        }

        private class Provider : IProvider
        {
            public SharedPtr<ExampleInjectable> injectable;

            public Provider()
            {
                injectable = SharedPtr.Create(this, new ExampleInjectable());
            }

            public void Provide(IList<ISharedPtr> ptrs)
            {
                ptrs.Add(injectable);
            }
        }

        private class Consumer : IConsumer, IEnumerable<ISharedPtr>
        {
            public SharedPtr<ExampleInjectable> injectable, injectable1;

            public Consumer(IProvider provider)
            {
                provider.Inject(this);
                provider.Inject(this, out injectable1);
            }

            public void Inject(IList<ISharedPtr> ptrs)
            {
                injectable = ptrs.Find(ptr => ptr.Type == typeof(ExampleInjectable), null) as SharedPtr<ExampleInjectable>;
                injectable.BindUser(this);
            }

            public IEnumerator<ISharedPtr> GetEnumerator()
            {
                yield return injectable;
                yield return injectable1;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        private class AltConsumer : IDisposable
        {
            private SharedPtr<ExampleInjectable> ptr;

            public AltConsumer(IProvider provider)
            {
                provider.Inject(this, out ptr);

                DoSomethingInternal();
                DoSomethingCachedInternal();
            }

            public void Dispose()
            {
                // Does not delete the pointer, just free's our local copy...
                ptr = null;
            }

            private void DoSomethingInternal()
            {
                ptr[this].DoSomething();
            }

            private void DoSomethingCachedInternal()
            {
                // This should be a stack variable making it okay to cache locally here
                var obj = ptr[this];
                obj.DoSomething();
            }
        }
    }
}