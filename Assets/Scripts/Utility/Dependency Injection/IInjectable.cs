﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
namespace pod.dependancy_injection
{
	/// <summary>
	/// Supports being injected through use of <see cref="SharedPtr"/>s.
	/// </summary>
	public interface IInjectable
	{
	}
}