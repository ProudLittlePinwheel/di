﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
namespace pod.dependancy_injection
{
    /// <summary>
    /// Represents the lifetime of a shared pointer.
    /// </summary>
	public enum SharedPtrLifespan
	{
		/// <summary>
		/// A single valid Delete call frees the reference.
		/// </summary>
		SingleBinding,
		/// <summary>
		/// All bound users (including the owner) must call Delete or be unbound to free the reference.
		/// </summary>
		MultipleBinding,
		/// <summary>
		/// Will only be freed if the original owner has gone out of scope or it is the original owner that is calling Delete.
		/// </summary>
		OwnerOnly,
	}
}