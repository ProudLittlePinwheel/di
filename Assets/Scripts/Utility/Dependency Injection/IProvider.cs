﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pod.dependancy_injection
{
	/// <summary>
	/// Able to provide a list of <see cref="ISharedPtr"/>(s).
	/// </summary>
	public interface IProvider
	{
		/// <summary>
		/// Provides a list of <see cref="ISharedPtr"/>(s) that this <see cref="IProvider"/> supports injecting into other objects.
		/// </summary>
		/// <param name="ptrs"></param>
		void Provide(IList<ISharedPtr> ptrs);
	}
}