﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections.Generic;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Represents a singleton object. 
    /// <para/>Use <see cref="Singleton.Create{T}(T)"/> or alternatively create a <see cref="SharedPtr{T}"/> with <see cref="Singleton.Provider"/> as the owner to create a singleton.
    /// <para/>You can also turn an existing <see cref="SharedPtr{T}"/> into a <see cref="Singleton{T}"/> using <see cref="Singleton.MakeSingleton{T}(SharedPtr{T})"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public class Singleton<T> : ISingleton where T : class, IInjectable
    {
        private SharedPtr<T> ptr; // The underlying ptr object
        public SharedPtrLifespan Lifespan { get { return ptr.Lifespan; } } // Implementing ISharedPtr
#pragma warning disable 0693 // Type parameter has the same name as the type parameter from outer type
        public SharedPtr<T> Cast<T>() where T : class, IInjectable { return ptr.Cast<T>(); } // Implementing ISharedPtr
        public SharedPtr<T> Cast<T>(SharedPtr<T> t) where T : class, IInjectable { return ptr.Cast<T>(t); } // Implementing ISharedPtr
#pragma warning restore 0693 // Type parameter has the same name as the type parameter from outer type
        public Type Type { get { return ptr.Type; } } // Implementing ISharedPtr
        public bool IsUser(object user) { return ptr.IsUser(user); } // Implementing ISharedPtr

        /// <summary>
        /// Returns the instance pointed to by the singleton. Will throw a <see cref="MissingReferenceException"/> if the instance has been destroyed.
        /// </summary>
        public T Instance { get { return ptr[Singleton.Provider]; } }
        
        object ISingleton.GetInstanceAsObject()
        {
            return Instance;
        }

        // (Internal use only)
        private Singleton(SharedPtr<T> ptr)
        {
            if (ptr.IsSingleton)
            {
                UnsafeSharedPtrAccess._Init();
                throw new Exception();
            }
            this.ptr = ptr;
        }

        public Singleton<T> BindUser(object user)
        {
            ptr.BindUser(user);
            return this;
        }

        public static implicit operator SharedPtr<T>(Singleton<T> singleton)
        {
            return (object)singleton != null ? singleton.ptr : null;
        }

        public static implicit operator Singleton<T>(SharedPtr<T> ptr)
        {
            if ((object)ptr == null)
            {
                return null;
            }
            else if (!ptr.IsSingleton)
            {
                throw new InvalidCastException();
            }
            return ((Singleton)Singleton.Provider).Find<T>();
        }

        public static bool operator ==(Singleton<T> a, Singleton<T> b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (ReferenceEquals(a, null))
            {
                if (ReferenceEquals(b, null) || b.ptr == null)
                {
                    return true;
                }
                return false;
            }

            if (ReferenceEquals(b, null))
            {
                if (ReferenceEquals(a, null) || a.ptr == null)
                {
                    return true;
                }
                return false;
            }

            return a.ptr == b.ptr;
        }

        public static bool operator !=(Singleton<T> a, Singleton<T> b)
        {
            return !(a == b);
        }

        /// <summary>
        /// (Internal use only) Helper class for generating <see cref="Singleton{T}"/>s.
        /// </summary>
		public static class Factory
        {
            /// <summary>
            /// (Internal use only) Generates a <see cref="Singleton{T}"/> from a <see cref="SharedPtr{T}"/>.
            /// </summary>
            /// <param name="ptr"></param>
            /// <returns></returns>
			public static Singleton<T> Create(SharedPtr<T> ptr)
            {
                if (UnsafeSharedPtrAccess.Internal.AccessToken)
                {
                    SharedPtr<T> existing = ((Singleton)Singleton.Provider).Find<T>();
                    if ((object)existing != null) // Already within the provider list
                    {
                        // Check to see if the reference is null?
                        if (existing[null] == null && ptr[null] != null)
                        {
                            // This is valid, we treat this creation as a late bind to the Singleton system
                            existing.BindUser(new KeyValuePair<object, object>(Singleton.Provider, ptr[null]));
                            return null;
                        }
                    }
                    return new Singleton<T>(ptr);
                }
                else
                {
                    throw new MethodAccessException("You may not generate singletons via this method.");
                }
            }
        }
    }
}
