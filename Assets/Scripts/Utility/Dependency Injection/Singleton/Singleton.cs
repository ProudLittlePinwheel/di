﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Class that creates and catalogues singletons
    /// </summary>
    public class Singleton : IProvider
    {
        /// <summary>
        /// The global singlton provider object.
        /// </summary>
        public static IProvider Provider { get; private set; }
        private List<ISharedPtr> singletons; // Internal singletons list
        private Dictionary<Type, ISingleton> singletons_map;
        public void Provide(IList<ISharedPtr> ptrs)
        {
            if (UnsafeSharedPtrAccess.Internal.AccessToken)
            {
                // Inject through the ptrs field
                var type = ptrs[0].Type;
                if (singletons_map.ContainsKey(type))
                {
                    UnsafeSharedPtrAccess._Init();
                    throw new SingletonException();
                }
                // Passed ISharedPtr is actually Singleton<T>
                singletons.Add(ptrs[0]);
                singletons_map.Add(type, (ISingleton)ptrs[0]);
                ptrs[0].IsUser(Provider);
            }
            else
            {
                List<ISharedPtr> ptrs_list = ptrs as List<ISharedPtr>;
                if (ptrs_list != null)
                {
                    ptrs_list.AddRange(singletons);
                }
                else
                {
                    for (int i = 0; i < singletons.Count; ++i)
                    {
                        ptrs.Add(singletons[i]);
                    }
                }
            }
        }
        /// <summary>
        /// Find a singleton by type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Singleton<T> Find<T>() where T : class, IInjectable
        {
            var match = typeof(T);
            ISingleton singleton;
            singletons_map.TryGetValue(match, out singleton);
            return singleton == null ? null : (Singleton<T>)singleton;
        }

        // Not yet ready for production use
        public ISingleton Find(Type t)
        {
            var match = t;
            ISingleton singleton;
            singletons_map.TryGetValue(match, out singleton);
            return singleton;
        }
        /// <summary>
        /// Find a singleton and automatically bind user to it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="user"></param>
        /// <param name="singleton"></param>
        public static void Inject<T>(object user, out Singleton<T> singleton) where T : class, IInjectable
        {
            singleton = ((Singleton)Provider).Find<T>();
            if ((object)singleton != null)
            {
                ((SharedPtr<T>)singleton).BindUser(user);
            }
            else
            {
                // Ths singleton does not yet exists, so creat a deferred singleton
                singleton = SharedPtr.CreateDeferred<T>(Provider);
            }
        }
        /// <summary>
        /// Create a singleton representing the <paramref name="target"/> class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="target"></param>
        /// <returns></returns>
        public static Singleton<T> Create<T>(T target) where T : class, IInjectable
        {
            return SharedPtr.Create(Provider, target);
        }
        /// <summary>
        /// Turn a <see cref="SharedPtr{T}"/> into a <see cref="Singleton{T}"/>. Will return the appropriate representation if supplied <paramref name="sharedPtr"/> is already a <see cref="Singleton{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sharedPtr"></param>
		public static Singleton<T> MakeSingleton<T>(SharedPtr<T> sharedPtr) where T : class, IInjectable
        {
            if (!sharedPtr.IsSingleton)
            {
                UnsafeSharedPtrAccess.MakeSingleton(sharedPtr);
            }
            return (Singleton<T>)sharedPtr;
        }
        private Singleton()
        {
            singletons = new List<ISharedPtr>();
            singletons_map = new Dictionary<Type, ISingleton>();
        }
        static Singleton()
        {
            Provider = new Singleton();
        }

#if UNITY_EDITOR
        /// <summary>
        /// (Editor only) Helper function for resetting the global state of static Singletons.
        /// </summary>
        public static class Editor
        {
            /// <summary>
            /// (Editor only) Reset the internal singleton list and release references. Required if assembly reload is not performed between editor tests.
            /// </summary>
			public static void Reset()
            {
                ((Singleton)Provider).singletons.Clear();
                ((Singleton)Provider).singletons_map.Clear();
                Provider = new Singleton();
            }
        }
#endif
    }
}
