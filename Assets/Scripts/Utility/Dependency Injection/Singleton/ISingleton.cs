﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Abstract singleton representation
    /// </summary>
	public interface ISingleton : ISharedPtr
	{
        object GetInstanceAsObject();
	}
}
