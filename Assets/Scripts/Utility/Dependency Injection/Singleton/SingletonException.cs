﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Exception associated with errors within the singleton system.
    /// </summary>
    public class SingletonException : Exception
    {
        public SingletonException() { }
        public SingletonException(string message) : base(message) { }
    }
}
