﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Exception that is thrown when an invalid user is supplied to a shared pointer.
    /// </summary>
	public class InvalidSharedPtrUserException : Exception
	{
		public InvalidSharedPtrUserException() : base() { }
		public InvalidSharedPtrUserException(string message) : base(message) { }
	}
}