﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
#pragma warning disable 0661 // Type defines operator == or operator != but does not override Object.GetHashCode()
#pragma warning disable 0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Pointer that wraps an object reference.
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public sealed class SharedPtr<T> : ISharedPtr where T : class, IInjectable
    {
        // TODO: Push users list here and remove static dictionary, that way if we go out of scope, it will be cleaned properly
        // also will not have risk of collisions on ids

        private WeakReference owner;
        private List<WeakReference> ref_list;

        // ID -> ID_List<string> 

        // The private target reference
        private T target;

        /// <summary>
        /// Unique id of this shared pointer container.
        /// </summary>
        public string Id { get { return id; } }
        private string id;

        /// <summary>
        /// The underlying type of this pointer.
        /// </summary>
        public Type Type { get { return typeof(T); } }
        /// <summary>
        /// Helper function to try and cast this <see cref="ISharedPtr"/> to <see cref="SharedPtr{T}"/>. Uses <paramref name="t"/> as a hint to determine <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
		public SharedPtr<U> Cast<U>() where U : class, IInjectable
        {
            return (SharedPtr<U>)(ISharedPtr)this;
        }
        /// <summary>
        /// Helper function to try and cast this <see cref="ISharedPtr"/> to <see cref="SharedPtr{T}"/>. Uses <paramref name="t"/> as a hint to determine <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public SharedPtr<U> Cast<U>(SharedPtr<U> u) where U : class, IInjectable
        {
            return (SharedPtr<U>)(ISharedPtr)this;
        }
        public static implicit operator Type(SharedPtr<T> ptr)
        {
            return typeof(T);
        }

        // The reference that the shared pointer points to
        //public T Reference { get { return target; } }
        /// <summary>
        /// Dereference the pointer with respect to the user. Will throw an exception for invalid users.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public T this[object user]
        {
            get
            {
                if (UnsafeSharedPtrAccess.Internal.AccessToken) // This is universal access
                    return target;

                // TODO: Check if owner is null, and throw if it is (may allow in later version)
                if (user == null)
                    throw new InvalidSharedPtrUserException();

                if (target == null)
                    throw new MissingReferenceException();

                var list = ref_list;
                bool changed = false;
                bool validUser = false;
                for (int i = 0; i < list.Count; ++i)
                {
                    if (list[i].Target == null)
                    {
                        ref_pool.Return(list[i]);
                        list[i] = null;
                        changed = true;
                    }
                    else if (list[i].Target == user)
                    {
                        validUser = true;
                    }
                }
                if (changed)
                {
                    var new_list = list_ref_pool.Get();
                    for (int i = 0; i < list.Count; ++i)
                    {
                        if (list[i] != null)
                            new_list.Add(list[i]);
                    }
                    ref_list = new_list;
                    list_ref_pool.Return(list);
                }

                if (validUser)
                    return target;
                throw new InvalidSharedPtrUserException();
            }
        }

        /// <summary>
        /// Lifespan of the shared pointer.
        /// </summary>
		public SharedPtrLifespan Lifespan { get; private set; }

        /// <summary>
        /// Create a pointer to the reference.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
		public static SharedPtr<T> Create(object owner, T reference)
        {
            return new SharedPtr<T>(owner, reference, SharedPtrLifespan.SingleBinding);
        }

        /// <summary>
        /// Create a pointer to the reference with a specified lifetime.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="reference"></param>
        /// <param name="lifespan"></param>
        /// <returns></returns>
		public static SharedPtr<T> Create(object owner, T reference, SharedPtrLifespan lifespan)
        {
            return new SharedPtr<T>(owner, reference, lifespan);
        }

        // Should be able to bind only one owner to the shared pointer
        private SharedPtr(object owner, T reference, SharedPtrLifespan lifespan)
        {
            if (owner == null)
            {
                UnsafeSharedPtrAccess._Init(); // We must reset the access token before throwing
                throw new NullReferenceException();
            }

            if (!UnsafeSharedPtrAccess.Internal.AccessToken)
            {
                if (reference == null)
                    throw new NullReferenceException("The reference must not be null when creating with a constructor. Perhaps you should use SharedPtr.CreateDeferred instead.");
            }

            if (owner == reference)
                throw new InvalidSharedPtrUserException();

            id = Guid.NewGuid().ToString();
            this.owner = ref_pool.Get();
            this.owner.Target = owner;

            target = reference;
            var list = list_ref_pool.Get();
            var weak_ref = ref_pool.Get();
            weak_ref.Target = owner;
            list.Add(weak_ref);
            ref_list = list;

            if (owner is Singleton)
            {
                Singleton.MakeSingleton(this);
            }
        }

        /// <summary>
        /// Allows binding a user to this shared pointer so that it can be dereferenced.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public SharedPtr<T> BindUser(object user)
        {
            // When the access token is used we expect a late target to bind to
            if (UnsafeSharedPtrAccess.Internal.AccessToken)
            {
                var pair = (KeyValuePair<object, object>)user;
                if (target != null || ref_list != null)
                {
                    var list = ref_list;
                    if (IsNewUser(pair.Key, list))
                    {
                        UnsafeSharedPtrAccess._Init(); // We must reset the access token before throwing
                        throw new InvalidSharedPtrUserException();
                    }
                }
                else
                {
                    owner = ref_pool.Get();
                    owner.Target = pair.Key;
                    var list = list_ref_pool.Get();
                    var weak_ref = ref_pool.Get();
                    weak_ref.Target = pair.Key;
                    list.Add(weak_ref);
                    ref_list = list;
                }
                target = (T)pair.Value;
                return this;
            }

            // TODO: check if owner not null, throw if owner is null
            if (user == null)
                throw new InvalidSharedPtrUserException();

            //if (target == null)
            //	throw new NullReferenceException();

            if (user == target)
                throw new InvalidSharedPtrUserException();

            if (ref_list != null)
            {
                var list = ref_list;
                if (!IsNewUser(user, list))
                {
                    //throw new InvalidSharedPtrUserException();
                    return this;
                }

                var weak_ref = ref_pool.Get();
                weak_ref.Target = user;
                list.Add(weak_ref);
            }

            return this;
        }

        private bool IsNewUser(object user, List<WeakReference> list)
        {
            bool changed = false;
            bool newUser = true;
            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].Target == null)
                {
                    ref_pool.Return(list[i]);
                    list[i] = null;
                    changed = true;
                }
                else if (list[i].Target == user)
                {
                    newUser = false;
                }
            }
            if (changed)
            {
                var new_list = list_ref_pool.Get();
                for (int i = 0; i < list.Count; ++i)
                {
                    if (list[i] != null)
                        new_list.Add(list[i]);
                }
                ref_list = new_list;
                list_ref_pool.Return(list);
            }
            return newUser;
        }

        /// <summary>
        /// Determine if <paramref name="user"/> is tied to this shared pointer.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
		public bool IsUser(object user)
        {
            if (UnsafeSharedPtrAccess.Internal.AccessToken)
            {
                if (user is Singleton)
                {
                    // Bind as a singleton
                    owner.Target = user;
                    IsSingleton = true;
                    return true;
                }
            }

            return !IsNewUser(user, ref_list);
        }

        /// <summary>
        /// Delete the reference to which this points and removes user binding information.
        /// </summary>
        /// <param name="user"></param>
		public void Delete(object user)
        {
            Delete(user, true);
        }

        /// <summary>
        /// Delete the reference to which this points. If bindings are not removed then a deferred bind can still be performed.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="removeBindings"></param>
		public void Delete(object user, bool removeBindings)
        {
            var @ref = this[user]; // Will throw if invalid user tries to access
            var list = ref_list;
            
            // Log who invoked the delete event, whether they owned it, how many people still point to it, etc...
            Debug.Log("Deleting <" + target.GetType() + "> from <" + user.GetType() + "> while [" + list.Count(wr => wr != null && wr.Target != null) + "] object(s) point to it.");

            switch (Lifespan)
            {
                case SharedPtrLifespan.MultipleBinding:
                    {
                        /// TODO: Make this remove the owner reference correctly...
                        for (int i = 0; i < list.Count; ++i)
                        {
                            if (list[i].Target == user)
                            {
                                ref_pool.Return(list[i]);
                                list.RemoveAt(i);
                                break;
                            }
                        }
                        if (list.Count == 0)
                        {
                            goto case SharedPtrLifespan.SingleBinding;
                        }
                    }
                    break;
                case SharedPtrLifespan.OwnerOnly:
                    {
                        if (user == owner.Target || owner.Target == null)
                            goto case SharedPtrLifespan.SingleBinding;
                        throw new InvalidSharedPtrUserException("A Shared Pointer User cannot destroy a Shared Pointer with Lifespan of OwnerOnly.");
                    }
                case SharedPtrLifespan.SingleBinding:
                    {
                        target = null;

                        if (removeBindings)
                        {
                            RemoveBindings(true);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Remove the users bound to this pointer.
        /// </summary>
        public void ResetBindings()
        {
            RemoveBindings(false);
        }

        private void RemoveBindings(bool deleteOwner)
        {
            // Remove from the ref_map
            if (deleteOwner)
            {
                ref_pool.Return(owner);
                owner = null;
            }
            var list = ref_list;
            for (int i = 0; i < list.Count; ++i)
            {
                list[i].Target = null;
                ref_pool.Return(list[i]);
            }
            list_ref_pool.Return(list);
            ref_list = null;
        }

        /// <summary>
        /// Determine if this shared pointer is a singleton.
        /// </summary>
        public bool IsSingleton { get; private set; }

        public static bool operator ==(SharedPtr<T> a, SharedPtr<T> b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if (ReferenceEquals(a, null))
            {
                if (ReferenceEquals(b, null) || b.target == null)
                {
                    return true;
                }
                return false;
            }

            if (ReferenceEquals(b, null))
            {
                if (ReferenceEquals(a, null) || a.target == null)
                {
                    return true;
                }
                return false;
            }

            return a.target == b.target;
        }

        public static bool operator !=(SharedPtr<T> a, SharedPtr<T> b)
        {
            return !(a == b);
        }

        public static bool operator ==(SharedPtr<T> a, object b)
        {
            if (!ReferenceEquals(a, null))
            {
                return a.Equals(b);
            }
            return b == null;
        }

        public static bool operator !=(SharedPtr<T> a, object b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="object"/>.</param>
        /// <returns>true if the specified <see cref="object"/> is equal to the current <see cref="object"/>; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null))
            {
                return this == null;
            }

            var other = obj as SharedPtr<T>;
            if (other != null)
            {
                return other == this;
            }

            var otherT = obj as T;
            if (otherT != null)
            {
                return target == otherT;
            }

            return base.Equals(obj);
        }

        // TODO: Should the dictionary really add the owner as a user...
        //private static Dictionary<string, List<WeakReference>> ref_map = new Dictionary<string, List<WeakReference>>();
        private static FactoryPool<WeakReference> ref_pool = new FactoryPool<WeakReference>(() => new WeakReference(null), null, wr => wr.Target = null);
        private static Pool<List<WeakReference>> list_ref_pool = new Pool<List<WeakReference>>(null, list => list.Clear());
    }
}
#pragma warning restore 0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
#pragma warning restore 0661 // Type defines operator == or operator != but does not override Object.GetHashCode()