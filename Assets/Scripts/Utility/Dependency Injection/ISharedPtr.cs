﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Shared pointer interface.
    /// </summary>
	public interface ISharedPtr
	{
        /// <summary>
        /// Lifespan of the shared pointer.
        /// </summary>
		SharedPtrLifespan Lifespan { get; }
        /// <summary>
        /// Helper function to try and cast this <see cref="ISharedPtr"/> to <see cref="SharedPtr{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
		SharedPtr<T> Cast<T>() where T : class, IInjectable;
        /// <summary>
        /// Helper function to try and cast this <see cref="ISharedPtr"/> to <see cref="SharedPtr{T}"/>. Uses <paramref name="t"/> as a hint to determine <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        SharedPtr<T> Cast<T>(SharedPtr<T> t) where T : class, IInjectable;
        /// <summary>
        /// The underlying type of this pointer.
        /// </summary>
		Type Type { get; }
        /// <summary>
        /// Determine if <paramref name="user"/> is tied to this shared pointer.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
		bool IsUser(object user);
	}
}