﻿/*Copyright (c) 2018 Robert Laird <robert.f.a.laird@gmail.com> MIT License https://opensource.org/licenses/MIT */
using System;
using System.Collections.Generic;

namespace pod.dependancy_injection
{
    /// <summary>
    /// Provides unsafe untility functions for performing primitve operations on <see cref="SharedPtr{T}"/>s. 
    /// Generally recommended for internal use only.
    /// </summary>
	public static class UnsafeSharedPtrAccess
    {
        public static class Internal
        {
            /// <summary>
            /// (Readonly) A token used for accessing internal functionality of the di system 
            /// </summary>
			public static bool AccessToken { get { return mAccessToken; } }
        }

        private static bool mAccessToken;

        /// <summary>
        /// (Internal use only) Reset the state of the static <see cref="UnsafeSharedPtrAccess"/> object.
        /// </summary>
		public static void _Init() { mAccessToken = false; }

        /// <summary>
        /// (Unsafe) Return the internal value held by the <see cref="SharedPtr{T}"/> which, if null, will not throw an exception.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ptr">The pointer.</param>
        /// <returns></returns>
		public static T RawPtr<T>(SharedPtr<T> ptr) where T : class, IInjectable
        {
            mAccessToken = true;
            T value = ptr[null];
            mAccessToken = false;
            return value;
        }

        /// <summary>
        /// (Unsafe) Bind a target to an existing <see cref="SharedPtr{T}"/> of the same type. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="user">A valid user of the pointer.</param>
        /// <param name="target">A non-null target to bind the pointer to.</param>
        /// <param name="ptr">The pointer.</param>
		public static void BindTarget<T>(object user, T target, SharedPtr<T> ptr) where T : class, IInjectable
        {
            mAccessToken = true;
            ptr.BindUser(new KeyValuePair<object, object>(user, target));
            mAccessToken = false;
        }

        /// <summary>
        /// (Unsafe) Create a null target <see cref="SharedPtr{T}"/> with the supplied <see cref="object"/> as the owner.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
		public static SharedPtr<T> CreateNullTarget<T>(object owner) where T : class, IInjectable
        {
            mAccessToken = true;
            var ptr = SharedPtr<T>.Create(owner, null);
            mAccessToken = false;
            return ptr;
        }

        private static Pool<List<ISharedPtr>> ptrs = new Pool<List<ISharedPtr>>(null, list => list.Clear());
        /// <summary>
        /// (Unsafe) Generate a <see cref="Singleton{T}"/> from a valid <see cref="SharedPtr{T}"/>. 
        /// <para/>The function will throw a <see cref="SingletonException"/> if a singleton of type T already exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ptr"></param>
		public static void MakeSingleton<T>(SharedPtr<T> ptr) where T : class, IInjectable
        {
            var list = ptrs.Get();
            mAccessToken = true;
			// The factory will throw back a null value if the singleton is already created as a deferred singleton
			// Therefore should just bind to it (via IsUser unsafe access) rather than add into list.
            var singleton = Singleton<T>.Factory.Create(ptr);
            if ((object)singleton == null)
            {
                ptr.IsUser(Singleton.Provider);
            }
            else
            {
                // Inject into provide function during unsafe access adds to Provider's singleton map
                list.Add(singleton);
                Singleton.Provider.Provide(list);
            }
            mAccessToken = false;
            ptrs.Return(list);
        }
    }
}