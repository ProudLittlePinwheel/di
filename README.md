# DI

Simple reflectionless dependency injection framework for use with Unity projects. 

Written as an experiment over christmas 2017-2018.

Supports enumerated dependency export by classes, deferred dependency creation (in relation to registering dependency interest) and singleton support. 

See DependancyInjectionExample.cs and DependancyInjectionTest.cs for example usage.